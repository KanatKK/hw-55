import React, {useState} from 'react';
import './App.css';
import Menu from "../../components/menu";
import Ingredients from "../../components/ingridients";

  const App = () => {
    const [menu, setMenu] = useState([
      {className: 'ingredient', name: 'meat', counter: 0, delete:'deleteMeat'},
      {className: 'ingredient', name: 'cheese', counter: 0, delete:'deleteCheese'},
      {className: 'ingredient', name: 'salad', counter: 0, delete:'deleteSalad'},
      {className: 'ingredient', name: 'bacon', counter: 0, delete:'deleteBacon'},
    ]);

    const[ingredients, setIngredients] = useState([]);

    const addIngredient = index => {
      const ingredientsCopy = [...ingredients]
      const push = (newIndex, name, currentPrice) => {
        if (index === newIndex) {
          ingredientsCopy.push({className: name, price: currentPrice})
        }
      }
      push(0, "Meat",50)
      push(1, "Cheese", 20)
      push(2, "Salad", 5)
      push(3, "Bacon", 30)
      setIngredients(ingredientsCopy);
      const menuCopy = [...menu];
      const item = {...menu[index]}
      item.counter++;
      menuCopy[index] = item;
      setMenu(menuCopy);
    };

    const deleteIngredient = index => {
      const ingredientsCopy = [...ingredients];
      const deleteIngredients = (newIndex , name) => {
        if (index === newIndex) {
          for (const key in ingredientsCopy) {
            if (ingredientsCopy[key].className === name) {
              ingredientsCopy.splice(Number(key), 1)
            }
          }
        }
      }
      deleteIngredients(0, 'Meat');
      deleteIngredients(1, 'Cheese');
      deleteIngredients(2, 'Salad');
      deleteIngredients(3, 'Bacon');
      const menuCopy = [...menu];
      const quantityOfMeat = ingredientsCopy.reduce((acc, quantity) => {
        if (quantity.className.indexOf('Meat') !== -1) {
          return acc + 1
        }
        return acc
      },0);
      const quantityOfCheese= ingredientsCopy.reduce((acc, quantity) => {
        if (quantity.className.indexOf('Cheese') !== -1) {
          return acc + 1
        }
        return acc
      },0);
      const quantityOfSalad = ingredientsCopy.reduce((acc, quantity) => {
        if (quantity.className.indexOf('Salad') !== -1) {
          return acc + 1
        }
        return acc
      },0);
      const quantityOfBacon = ingredientsCopy.reduce((acc, quantity) => {
        if (quantity.className.indexOf('Bacon') !== -1) {
          return acc + 1
        }
        return acc
      },0);
      menuCopy[0].counter = quantityOfMeat;
      menuCopy[1].counter = quantityOfCheese;
      menuCopy[2].counter = quantityOfSalad;
      menuCopy[3].counter = quantityOfBacon;
      setMenu(menuCopy);
      setIngredients(ingredientsCopy)
    };

    let totalPrice = () => {
      return ingredients.reduce((acc, item) => {
        return acc + item.price
      }, 0);
    };

    let menuList = menu.map((menu, index) => {
      return(
          <Menu className={menu.className} name={menu.name}
                counter={menu.counter} delete={menu.delete} key={index}
                onIngredientClick={() => addIngredient(index)}
                deleteIngredient={() => deleteIngredient(index)}/>
      );
    });

    let ingredientsList = ingredients.map((ingredients, index) => {
      return (
          <Ingredients className={ingredients.className} key={index}/>
      );
    });

    return(
        <div className="container">
          <div className="ingredients">
            <h1>Ingredients</h1>
            {menuList}
          </div>
          <div className="burgerImg">
            <h1 className="burgerHeading">Burger</h1>
            <div className="Burger">
              <div className="BreadTop">
                <div className="Seeds1"/>
                <div className="Seeds2"/>
              </div>
              {ingredientsList}
              <div className="BreadBottom"/>
            </div>
            <p className="price">Price: {totalPrice() + 20}</p>
          </div>
        </div>
    )
}

export default App;
