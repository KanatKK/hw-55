import React from 'react';

const Menu = props => {
    return (
        <div className={props.className}>
            <div className={props.name} onClick={props.onIngredientClick}>
                <p className="ingredientName">{props.name}</p>
            </div>
            <span className="counter">x {props.counter}</span>
            <span className={props.delete} onClick={props.deleteIngredient}/>
        </div>
    );
};

export default Menu;